\select@language {danish}
\contentsline {chapter}{\numberline {1}Psykologien Som Videnskab}{4}{chapter.1}
\contentsline {section}{\numberline {1.1}Introduktion}{5}{section.1.1}
\contentsline {section}{\numberline {1.2}Hverdagspsykologi vs. Pop-psykologi}{5}{section.1.2}
\contentsline {section}{\numberline {1.3}Den videnskabelige psykologi}{5}{section.1.3}
\contentsline {subsection}{\numberline {1.3.1}Hvad er videnskab?}{6}{subsection.1.3.1}
\contentsline {subsection}{\numberline {1.3.2}Den videnskabelige fremgangsm\IeC {\r a}de}{6}{subsection.1.3.2}
\contentsline {subsubsection}{Hypoteseafpr\IeC {\o }vning}{6}{section*.2}
\contentsline {subsection}{\numberline {1.3.3}Fors\IeC {\o }gsledereffekten}{6}{subsection.1.3.3}
\contentsline {section}{\numberline {1.4}De vigtigste psykologiske tilgange}{7}{section.1.4}
\contentsline {section}{\numberline {1.5}Gennemg\IeC {\r a}ende temaer i psykologien}{7}{section.1.5}
\contentsline {chapter}{\numberline {2}Personlighed}{9}{chapter.2}
\contentsline {section}{\numberline {2.1}Introduktion}{10}{section.2.1}
\contentsline {section}{\numberline {2.2}Freud og psykoanalysen}{10}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Den psykoseksuelle udvikling}{10}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Freuds personlighedsmodel}{11}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}\IeC {\O }dipuskomplekset}{11}{subsection.2.2.3}
\contentsline {subsection}{\numberline {2.2.4}Elektrakomplekset}{12}{subsection.2.2.4}
\contentsline {subsection}{\numberline {2.2.5}Trodsalderen}{12}{subsection.2.2.5}
\contentsline {subsection}{\numberline {2.2.6}Forsvarsmekanismer}{12}{subsection.2.2.6}
\contentsline {subsubsection}{Liste over forsvarsmekanismer}{13}{section*.3}
\contentsline {section}{\numberline {2.3}Carl Jung}{15}{section.2.3}
\contentsline {section}{\numberline {2.4}Humanistisk psykologi}{15}{section.2.4}
\contentsline {section}{\numberline {2.5}Tr\IeC {\ae }kteorier}{16}{section.2.5}
\contentsline {section}{\numberline {2.6}The big five}{16}{section.2.6}
\contentsline {section}{\numberline {2.7}Pensum/film}{17}{section.2.7}
\contentsline {subsection}{\numberline {2.7.1}Dennis}{17}{subsection.2.7.1}
\contentsline {subsection}{\numberline {2.7.2}Festen}{17}{subsection.2.7.2}
\contentsline {section}{\numberline {2.8}Begreber}{18}{section.2.8}
\contentsline {chapter}{\numberline {3}Adf\IeC {\ae }rdspsykologi}{20}{chapter.3}
\contentsline {section}{\numberline {3.1}Introduktion}{21}{section.3.1}
\contentsline {section}{\numberline {3.2}Klassisk betingning \IeC {\textendash } Pavlovs hunde}{21}{section.3.2}
\contentsline {section}{\numberline {3.3}Watson og Lille Albert}{21}{section.3.3}
\contentsline {section}{\numberline {3.4}Operant betingning - Skinner}{21}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Forst\IeC {\ae }rkning og straf}{22}{subsection.3.4.1}
\contentsline {subsubsection}{Positiv og negativ forst\IeC {\ae }rkning}{22}{section*.4}
\contentsline {subsubsection}{Straf}{22}{section*.5}
\contentsline {section}{\numberline {3.5}Begreber}{22}{section.3.5}
\contentsline {chapter}{\numberline {4}Kognitionspsykologi}{24}{chapter.4}
\contentsline {section}{\numberline {4.1}Introduktion}{25}{section.4.1}
\contentsline {section}{\numberline {4.2}Den dominerende psykologi}{25}{section.4.2}
\contentsline {section}{\numberline {4.3}Perception}{25}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Konstruktion af indtryk}{25}{subsection.4.3.1}
\contentsline {subsubsection}{Pr\IeC {\"a}gnanz}{26}{section*.6}
\contentsline {subsubsection}{Gestaltlovene}{26}{section*.7}
\contentsline {section}{\numberline {4.4}Kognitive skemaer}{26}{section.4.4}
\contentsline {section}{\numberline {4.5}T\IeC {\ae }ndthed og opm\IeC {\ae }rksomhed}{26}{section.4.5}
\contentsline {subsection}{\numberline {4.5.1}Arousal}{26}{subsection.4.5.1}
\contentsline {subsection}{\numberline {4.5.2}Opm\IeC {\ae }rksomhed}{27}{subsection.4.5.2}
\contentsline {section}{\numberline {4.6}Hukommelsen}{27}{section.4.6}
\contentsline {subsection}{\numberline {4.6.1}Sansehukommelsen}{28}{subsection.4.6.1}
\contentsline {subsection}{\numberline {4.6.2}Korttidshukommelsen}{28}{subsection.4.6.2}
\contentsline {subsection}{\numberline {4.6.3}Arbejdshukommelsen}{28}{subsection.4.6.3}
\contentsline {subsection}{\numberline {4.6.4}Langtidshukommelsen}{28}{subsection.4.6.4}
\contentsline {subsubsection}{Procedurehukommelsen}{28}{section*.8}
\contentsline {subsection}{\numberline {4.6.5}Hvorfor vi glemmer}{28}{subsection.4.6.5}
\contentsline {section}{\numberline {4.7}Intelligens}{29}{section.4.7}
\contentsline {subsection}{\numberline {4.7.1}Gardners intelligenser}{29}{subsection.4.7.1}
\contentsline {section}{\numberline {4.8}L\IeC {\ae }ring}{30}{section.4.8}
\contentsline {subsection}{\numberline {4.8.1}Typer af l\IeC {\ae }ring}{30}{subsection.4.8.1}
\contentsline {chapter}{\numberline {5}Udviklingspsykologi}{32}{chapter.5}
\contentsline {section}{\numberline {5.1}Introduktion}{33}{section.5.1}
\contentsline {section}{\numberline {5.2}Piaget}{34}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Kritik af Piaget}{34}{subsection.5.2.1}
\contentsline {section}{\numberline {5.3}Erik Erikson}{35}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Den psykosociale udvikling}{36}{subsection.5.3.1}
\contentsline {section}{\numberline {5.4}Tilknytning (Bowlby)}{37}{section.5.4}
\contentsline {section}{\numberline {5.5}Vigtige navne}{37}{section.5.5}
\contentsline {subsection}{\numberline {5.5.1}Ren\IeC {\'e} Spitz}{37}{subsection.5.5.1}
\contentsline {subsection}{\numberline {5.5.2}Harry Harlow}{37}{subsection.5.5.2}
\contentsline {section}{\numberline {5.6}Omsorgssvigt}{37}{section.5.6}
\contentsline {section}{\numberline {5.7}Social arv}{38}{section.5.7}
\contentsline {section}{\numberline {5.8}Resiliens og m\IeC {\o }nsterbrud}{39}{section.5.8}
\contentsline {chapter}{\numberline {6}Socialpsykologi}{40}{chapter.6}
\contentsline {section}{\numberline {6.1}Introduktion}{41}{section.6.1}
\contentsline {section}{\numberline {6.2}Selvet og selvopfattelsen}{41}{section.6.2}
\contentsline {section}{\numberline {6.3}Sociale grupper}{41}{section.6.3}
\contentsline {subsection}{\numberline {6.3.1}Forskellige slags grupper}{41}{subsection.6.3.1}
\contentsline {subsection}{\numberline {6.3.2}Gruppers sociale indflydelse}{42}{subsection.6.3.2}
\contentsline {section}{\numberline {6.4}Ondskab}{42}{section.6.4}
\contentsline {section}{\numberline {6.5}Begreber}{43}{section.6.5}
